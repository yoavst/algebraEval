package com.yoavst.algebra

import com.yoavst.algebra.parser.ExpressionParser
import org.jetbrains.spek.api.Given
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.shouldEqual
import kotlin.test.assertEquals

public class RpnTest : Spek() {
    init {
        given("an RPN converter") {
            simpleTest()
            functionsTest()
            unaryOperatorTest()
            variablesTests()
        }
    }

    fun Given.simpleTest() {
        on("""trying to convert the expression: "1+2"""") {
            it("""should be equals to "1 2 +" """) {
                shouldEqual("1 2 +", ExpressionParser("1+2").toRpnString())
            }
        }
        on("""trying to convert the expression: "1+2/4"""") {
            it("""should be equals to "1 2 4 / +" """) {
                shouldEqual("1 2 4 / +", ExpressionParser("1+2/4").toRpnString())
            }
        }
        on("""trying to convert the expression: "(1+2)/4"""") {
            it("""should be equals to "1 2 + 4 /" """) {
                shouldEqual("1 2 + 4 /", ExpressionParser("(1+2)/4").toRpnString())
            }
        }
        on("""trying to convert the expression: "(1.9+2.8)/4.7"""") {
            it("""should be equals to "1.9 2.8 + 4.7 /" """) {
                shouldEqual("1.9 2.8 + 4.7 /", ExpressionParser("(1.9+2.8)/4.7").toRpnString())
            }
        }
        on("""trying to convert the expression: "(1.98+2.87)/4.76"""") {
            it("""should be equals to "1.98 2.87 + 4.76 /"" """) {
                shouldEqual("1.98 2.87 + 4.76 /", ExpressionParser("(1.98+2.87)/4.76").toRpnString())
            }
        }
        on("""trying to convert the expression: "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3"""") {
            it("""should be equals to "3 4 2 * 1 5 - 2 3 ^ ^ / +" """) {
                shouldEqual("3 4 2 * 1 5 - 2 3 ^ ^ / +", ExpressionParser("3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3").toRpnString())
            }
        }

        on("""trying to convert the expression: "5(1+2)"""") {
            it("""should be equals to "5 1 2 + *" """) {
                shouldEqual("5 1 2 + *", ExpressionParser("5(1+2)").toRpnString())
            }
        }

        on("""trying to convert the expression: "-3(4)"""") {
            it("""should be equals to "3 - 4 *" """) {
                shouldEqual("3 - 4 *", ExpressionParser("-3(4)").toRpnString())
            }
        }

        on("""trying to convert the expression: "--+3(4)"""") {
            it("""should be equals to "3 + - - 4 *" """) {
                shouldEqual("3 + - - 4 *", ExpressionParser("--+3(4)").toRpnString())
            }
        }

        on("""trying to convert the expression: "-(4)"""") {
            it("""should be equals to "4 -" """) {
                shouldEqual("4 -", ExpressionParser("-(4)").toRpnString())
            }
        }

    }

    fun Given.functionsTest() {
        on("""trying to convert the expression: "SIN(23.6)"""") {
            it("""should be equals to "23.6 SIN" """) {
                shouldEqual("23.6 SIN", ExpressionParser("SIN(23.6)").toRpnString())
            }
        }
        on("""trying to convert the expression: "MAX(7,8)"""") {
            it("""should be equals to "7 8 MAX" """) {
                shouldEqual("7 8 MAX", ExpressionParser("MAX(7,8)").toRpnString())
            }
        }
        on("""trying to convert the expression: "MAX(SIN(3.7),MAX(2.6,8.0))"""") {
            it("""should be equals to "3.7 SIN 2.6 8.0 MAX MAX" """) {
                shouldEqual("3.7 SIN 2.6 8.0 MAX MAX", ExpressionParser("MAX(SIN(3.7),MAX(2.6,8.0))").toRpnString())
            }
        }

        on("""trying to convert the expression: "-3MAX(4)"""") {
            it("""should be equals to "3 - 4 MAX *" """) {
                shouldEqual("3 - 4 MAX *", ExpressionParser("-3MAX(4)").toRpnString())
            }
        }
    }

    fun Given.unaryOperatorTest() {
        on("""trying to convert the expression: "-54"""") {
            it("""should be equals to "54 -" """) {
                shouldEqual("54 -", ExpressionParser("-54").toRpnString())
            }
        }
        on("""trying to convert the expression: "42--35"""") {
            it("""should be equals to "42 35 - -" """) {
                shouldEqual("42 35 - -", ExpressionParser("42--35").toRpnString())
            }
        }

        on("""trying to convert the expression: "+SQRT(81) = 9"""") {
            it("""should be equals to "81 SQRT + 9 =" """) {
                shouldEqual("81 SQRT + 9 =", ExpressionParser("+SQRT(81) = 9").toRpnString())
            }
        }

        on("""trying to convert the expression: "-SQRT(81) = -9"""") {
            it("""should be equals to "81 SQRT - 9 - =" """) {
                shouldEqual("81 SQRT - 9 - =", ExpressionParser("-SQRT(81) = -9").toRpnString())
            }
        }

        on("""trying to convert the expression: "-------+-+-(5--4)-3"""") {
            it("""should be equals to "5 4 - - - + - + - - - - - - - 3 -" """) {
                shouldEqual("5 4 - - - + - + - - - - - - - 3 -", ExpressionParser("-------+-+-(5--4)-3").toRpnString())
            }
        }

        on("""trying to convert the expression: "MAX(SIN(3.7),MAX(-8.0, 2.6))"""") {
            it("""should be equals to "3.7 SIN 8.0 - 2.6 MAX MAX" """) {
                shouldEqual("3.7 SIN 8.0 - 2.6 MAX MAX", ExpressionParser("MAX(SIN(3.7),MAX(-8.0,2.6))").toRpnString())
            }
        }

        on("""trying to convert the expression: "-3+4"""") {
            it("""should be equals to "3 - 4 +" """) {
                shouldEqual("3 - 4 +", ExpressionParser("-3 + 4").toRpnString())
            }
        }

        on("""trying to convert the expression: "(-3+4)*-1/(7-(5*-8))"""") {
            it("""should be equals to "3 - 4 + 1 - * 7 5 8 - * - /" """) {
                shouldEqual("3 - 4 + 1 - * 7 5 8 - * - /", ExpressionParser("(-3+4)*-1/(7-(5*-8))").toRpnString())
            }
        }
    }

    fun Given.variablesTests() {
        on("""trying to convert the expression "5x + -3.5y = 3" """) {
            it("""should be equals to "5 x * 3.5 - y * + 3 ="""") {
                shouldEqual("5 x * 3.5 - y * + 3 =", ExpressionParser("5x + -3.5y = 3").toRpnString())
            }
        }
        on("""trying to convert the expression "3R2 + 4R1" """) {
            it("""should be equals to "3 R2 * 4 R1 * +"""") {
                shouldEqual("3 R2 * 4 R1 * +", ExpressionParser("3R2 + 4R1").toRpnString())
            }
        }

        on("""trying to convert the expression "3R2(4x-3) + 4R1" """) {
            it("""should be equals to "3 R2 * 4 x * 3 - * 4 R1 * +"""") {
                shouldEqual("3 R2 * 4 x * 3 - * 4 R1 * +", ExpressionParser("3R2(4x-3) + 4R1").toRpnString())
            }
        }

        on("""trying to convert the expression: "-3R2*MAX(4)"""") {
            it("""should be equals to "3 - R2 * 4 MAX *" """) {
                shouldEqual("3 - R2 * 4 MAX *", ExpressionParser("-3R2*MAX(4)").toRpnString())
            }
        }
    }
}
