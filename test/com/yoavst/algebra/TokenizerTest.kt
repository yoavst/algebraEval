package com.yoavst.algebra

import com.yoavst.algebra.parser.ExpressionParser
import com.yoavst.algebra.parser.Token
import com.yoavst.algebra.parser.TokenType
import org.jetbrains.spek.api.*
import kotlin.test.assertEquals
import kotlin.test.assertFalse


public class TokenizerTest : Spek() {
    init {
        given("an expression tokenizer") {
            testNumbers()
            testExtraSpaces()
            testGeneral()
            testFunctions()
            testVariables()
        }
    }

    fun Given.testNumbers() {
        on("trying to tokenize: 1") {
            val tokenizer = ExpressionParser("1").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 1") {
                shouldEqual(token.text, "1")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
        }

        on("trying to tokenize: 123") {
            val tokenizer = ExpressionParser("123").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 123") {
                shouldEqual(token.text, "123")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
        }

        on("trying to tokenize: -42") {
            val tokenizer = ExpressionParser("-42").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to -") {
                shouldEqual(token.text, "-")
            }
            it("should make the first token an unary operator") {
                shouldEqual(token.type, TokenType.UnaryOperator)
            }

            val secondToken = tokenizer.next()

            it("should make the second token equals to 42") {
                shouldEqual(secondToken.text, "42")
            }
            it("should make the second token a number") {
                shouldEqual(secondToken.type, TokenType.Number)
            }
        }

        on("trying to tokenize: +87") {
            val tokenizer = ExpressionParser("+87").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to +") {
                shouldEqual(token.text, "+")
            }
            it("should make the first token an unary operator") {
                shouldEqual(token.type, TokenType.UnaryOperator)
            }

            val secondToken = tokenizer.next()

            it("should make the second token equals to 87") {
                shouldEqual(secondToken.text, "87")
            }
            it("should make the second token a number") {
                shouldEqual(secondToken.type, TokenType.Number)
            }
        }

        on("trying to tokenize: +-143") {
            val tokenizer = ExpressionParser("+-143").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to +") {
                shouldEqual(token.text, "+")
            }
            it("should make the first token an unary operator") {
                shouldEqual(token.type, TokenType.UnaryOperator)
            }

            val secondToken = tokenizer.next()
            it("should make the second token equals to -") {
                shouldEqual(secondToken.text, "-")
            }
            it("should make the second token an unary operator") {
                shouldEqual(secondToken.type, TokenType.UnaryOperator)
            }
            val thirdToken = tokenizer.next()
            it("should make the third token equals to 143") {
                shouldEqual(thirdToken.text, "143")
            }
            it("should make the third token a number") {
                shouldEqual(thirdToken.type, TokenType.Number)
            }
        }

        on("trying to tokenize: 85.23") {
            val tokenizer = ExpressionParser("85.23").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 85.23") {
                shouldEqual(token.text, "85.23")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
        }
    }

    fun Given.testExtraSpaces() {
        on("trying to tokenize a string filled with spaces") {
            val tokenizer = ExpressionParser("       ").getTokenizer()
            it("Should have no tokens") {
                shouldBeFalse(tokenizer.hasNext())
                shouldThrow(javaClass<IllegalStateException>()) {
                    tokenizer.next()
                }
            }
        }

        on("trying to tokenize the string \"1\" with spaces after it") {
            val tokenizer = ExpressionParser("1 ").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 1") {
                shouldEqual(token.text, "1")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
            it("should have no extra tokens") {
                shouldBeFalse(tokenizer.hasNext())
                shouldThrow(javaClass<IllegalStateException>()) {
                    tokenizer.next()
                }
            }
        }

        on("trying to tokenize the string \"2\" with spaces before & after it") {
            val tokenizer = ExpressionParser("     1    ").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 1") {
                shouldEqual(token.text, "1")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
            it("should have no extra tokens") {
                shouldBeFalse(tokenizer.hasNext())
                shouldThrow(javaClass<IllegalStateException>()) {
                    tokenizer.next()
                }
            }
        }

        on("trying to tokenize the string \"1+2\" with some spaces") {
            val tokenizer = ExpressionParser("  1   +   2    ").getTokenizer()
            val token = tokenizer.next()
            it("should make the first token equals to 1") {
                shouldEqual(token.text, "1")
            }
            it("should make the first token a number") {
                shouldEqual(token.type, TokenType.Number)
            }
            val secondToken = tokenizer.next()
            it("should make the second token equals to +") {
                shouldEqual(secondToken.text, "+")
            }
            it("should make the second token an operator") {
                shouldEqual(secondToken.type, TokenType.Operator)
            }
            val thirdToken = tokenizer.next()
            it("should make the third token equals to 2") {
                shouldEqual(thirdToken.text, "2")
            }
            it("should make the third token a number") {
                shouldEqual(thirdToken.type, TokenType.Number)
            }
        }
    }

    fun Given.testGeneral() {
        on("""trying to parse: "1+2"""") {
            val tokenizer = ExpressionParser("1+2").getTokenizer()
            it("should parse it to 3 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2"), tokenizer.next())

            }
        }
        on("""trying to parse: "1 + 2"""") {
            val tokenizer = ExpressionParser("1 + 2").getTokenizer()
            it("should parse it to 3 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2"), tokenizer.next())

            }
        }
        on("""trying to parse: " 1 + 2 """"") {
            val tokenizer = ExpressionParser(" 1 + 2 ").getTokenizer()
            it("should parse it to 3 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2"), tokenizer.next())

            }
        }
        on("""trying to parse: "1+2-3/4*5"""") {
            val tokenizer = ExpressionParser("1+2-3/4*5").getTokenizer()
            it("should parse it to 9 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "4"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5"), tokenizer.next())
            }
        }
        on("""trying to parse: "1+2--3/4*5"""") {
            val tokenizer = ExpressionParser("1+2--3/4*5").getTokenizer()
            it("should parse it to 10 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "4"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5"), tokenizer.next())
            }
        }
        on(""""trying to parse: "1+2.1-3.45/4.982*+5.0"""") {
            val tokenizer = ExpressionParser("1+2.1-3.45/4.982*+5.0").getTokenizer()
            it("should parse it to 10 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "2.1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.45"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "4.982"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5.0"), tokenizer.next())
            }
        }
        on("""trying to parse: "-3+4*-1"""") {
            val tokenizer = ExpressionParser("-3+4*-1").getTokenizer()
            it("should parse it to 7 tokens correctly") {
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "4"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
            }
        }
        on(""""trying to parse: "(-3+4)*-1/(7-(5*-8))"""") {
            val tokenizer = ExpressionParser("(-3+4)*-1/(7-(5*-8))").getTokenizer()
            it("should parse it to 19 tokens correctly") {
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "4"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "1"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "7"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "*"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "8"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
            }
        }
    }

    fun Given.testFunctions() {
        on("""trying to parse "ABS(3.5)"""") {
            val tokenizer = ExpressionParser("ABS(3.5)").getTokenizer()

            it("should parse it to 4 tokens correctly") {
                shouldEqual(Token(TokenType.Name, "ABS"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
            }
        }

        on("""trying to parse "3-ABS(3.5)/9"""") {
            val tokenizer = ExpressionParser("3-ABS(3.5)/9").getTokenizer()

            it("should parse it to 8 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "ABS"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "9"), tokenizer.next())

            }
        }

        on("""trying to parse "MAX(3.5,5.2)"""") {
            val tokenizer = ExpressionParser("MAX(3.5,5.2)").getTokenizer()

            it("should parse it to 6 tokens correctly") {
                shouldEqual(Token(TokenType.Name, "MAX"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.Comma, ","), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5.2"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
            }
        }

        on("""trying to parse "3-MAX(3.5,5.2)/9"""") {
            val tokenizer = ExpressionParser("3-MAX(3.5,5.2)/9").getTokenizer()

            it("should parse it to 10 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "MAX"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.Comma, ","), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5.2"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "9"), tokenizer.next())
            }
        }

        on("""trying to parse "3/MAX(-3.5,-5.2)/9"""") {
            val tokenizer = ExpressionParser("3/MAX(-3.5,-5.2)/9").getTokenizer()

            it("should parse it to 12 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "MAX"), tokenizer.next())
                shouldEqual(Token(TokenType.LeftParen, "("), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.Comma, ","), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "5.2"), tokenizer.next())
                shouldEqual(Token(TokenType.RightParen, ")"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "/"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "9"), tokenizer.next())
            }
        }
    }

    fun Given.testVariables() {
        on("""trying to parse "5x + 4y = 3"""") {
            val tokenizer = ExpressionParser("5x + -3.5y = 3").getTokenizer()

            it("should parse it to 8 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "5"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "x"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3.5"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "y"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "="), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
            }
        }

        on("""trying to parse "5R2 + 3R1"""") {
            val tokenizer = ExpressionParser("5R2 + -3R1").getTokenizer()

            it("should parse it to 6 tokens correctly") {
                shouldEqual(Token(TokenType.Number, "5"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "R2"), tokenizer.next())
                shouldEqual(Token(TokenType.Operator, "+"), tokenizer.next())
                shouldEqual(Token(TokenType.UnaryOperator, "-"), tokenizer.next())
                shouldEqual(Token(TokenType.Number, "3"), tokenizer.next())
                shouldEqual(Token(TokenType.Name, "R1"), tokenizer.next())
            }
        }
    }
}
