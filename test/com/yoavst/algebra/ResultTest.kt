package com.yoavst.algebra

import com.yoavst.algebra.parser.ExpressionParser
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.shouldEqual
import org.jetbrains.spek.api.shouldNotEqual


public class ResultTest : Spek() {
    init {
        given("an expression evaluator") {
            on("receiving few expressions") {
                it("should solve them correctly") {
                    shouldEqual("3", ExpressionParser("1+2").eval().toPlainString())
                    shouldEqual("2", ExpressionParser("4/2").eval().toPlainString())
                    shouldEqual("5", ExpressionParser("3+4/2").eval().toPlainString())
                    shouldEqual("3.5", ExpressionParser("(3+4)/2").eval().toPlainString())
                    shouldEqual("7.98", ExpressionParser("4.2*1.9").eval().toPlainString())
                    shouldEqual("2", ExpressionParser("8%3").eval().toPlainString())
                    shouldEqual("0", ExpressionParser("8%2").eval().toPlainString())
                }
            }
            on("receiving expressions with pow") {
                it("should solve them correctly") {
                    shouldEqual("16",  ExpressionParser("2^4").eval().toPlainString())
                    shouldEqual("256",  ExpressionParser("2^8").eval().toPlainString())
                    shouldEqual("9",  ExpressionParser("3^2").eval().toPlainString())
                    shouldEqual("6.25",  ExpressionParser("2.5^2").eval().toPlainString())
                    shouldEqual("28.34045",  ExpressionParser("2.6^3.5").eval().toPlainString())
                }
            }

            on("receiving expressions with sqrt") {
                it("should solve them correctly") {
                    shouldEqual("4",  ExpressionParser("SQRT(16)").eval().toPlainString())
                    shouldEqual("-4",  ExpressionParser("-SQRT(16)").eval().toPlainString())
                    shouldEqual("1.4142135",  ExpressionParser("SQRT(2)").eval().toPlainString())
                    shouldEqual("1.41421356237309504880168872420969807856967187537694807317667973799073247846210703885038753432764157273501384623091229702492483605",  ExpressionParser("SQRT(2)").setPrecision(128).eval().toPlainString())
                    shouldEqual("2.2360679",  ExpressionParser("SQRT(5)").eval().toPlainString())
                    shouldEqual("99.3730345",  ExpressionParser("SQRT(9875)").eval().toPlainString())
                    shouldEqual("2.3558437",  ExpressionParser("SQRT(5.55)").eval().toPlainString())
                    shouldEqual("0",  ExpressionParser("SQRT(0)").eval().toPlainString())
                }
            }
            
            on("receiving expressions with functions") {
                it("should solve them correctly") {
                    shouldNotEqual("1.5",  ExpressionParser("RANDOM()").eval().toPlainString())
                    shouldEqual("0.400349",  ExpressionParser("SIN(23.6)").eval().toPlainString())
                    shouldEqual("8",  ExpressionParser("MAX(-7,8)").eval().toPlainString())
                    shouldEqual("5",  ExpressionParser("MAX(3,MAX(4,5))").eval().toPlainString())
                    shouldEqual("9.6",  ExpressionParser("MAX(3,MAX(MAX(9.6,-4.2),MIN(5,9)))").eval().toPlainString())
                    shouldEqual("2.302585",  ExpressionParser("LOG(10)").eval().toPlainString())
                }
            }
            
            on("receiving expressions with trigonometry") {
                it("should solve them correctly") {
                    shouldEqual("0.5",  ExpressionParser("SIN(30)").eval().toPlainString())
                    shouldEqual("0.8660254",  ExpressionParser("COS(30)").eval().toPlainString())
                    shouldEqual("0.5773503",  ExpressionParser("TAN(30)").eval().toPlainString())
                    shouldEqual("5343237000000",  ExpressionParser("SINH(30)").eval().toPlainString())
                    shouldEqual("5343237000000",  ExpressionParser("COSH(30)").eval().toPlainString())
                    shouldEqual("1",  ExpressionParser("TANH(30)").eval().toPlainString())
                    shouldEqual("0.5235988",  ExpressionParser("RAD(30)").eval().toPlainString())
                    shouldEqual("1718.873",  ExpressionParser("DEG(30)").eval().toPlainString())
                }
            }
            
            on("receiving expressions with rounding") {
                it("should round them correctly") {
                    shouldEqual("3.8",  ExpressionParser("round(3.78787,1)").eval().toPlainString())
                    shouldEqual("3.788",  ExpressionParser("round(3.78787,3)").eval().toPlainString())
                    shouldEqual("3.734",  ExpressionParser("round(3.7345,3)").eval().toPlainString())
                    shouldEqual("-3.734",  ExpressionParser("round(-3.7345,3)").eval().toPlainString())
                    shouldEqual("-3.79",  ExpressionParser("round(-3.78787,2)").eval().toPlainString())
                    shouldEqual("123.79",  ExpressionParser("round(123.78787,2)").eval().toPlainString())
                    shouldEqual("3",  ExpressionParser("floor(3.78787)").eval().toPlainString())
                    shouldEqual("4",  ExpressionParser("ceiling(3.78787)").eval().toPlainString())
                    shouldEqual("-3",  ExpressionParser("floor(-2.1)").eval().toPlainString())
                    shouldEqual("-2",  ExpressionParser("ceiling(-2.1)").eval().toPlainString())
                }
            }
        }
    }
}