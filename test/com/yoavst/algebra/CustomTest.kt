package com.yoavst.algebra

import com.yoavst.algebra.model.Operator
import com.yoavst.algebra.model.Function
import com.yoavst.algebra.model.UnaryOperator
import com.yoavst.algebra.parser.ExpressionParser
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.shouldEqual
import java.math.BigDecimal
import kotlin.math.div
import kotlin.math.plus

public class CustomTest : Spek() {
    init {
        given("an expression evaluator") {
            on("adding a custom operator") {
                val expression = ExpressionParser("2.1234 >> 2")
                expression.addOperator(object : Operator(">>", 30, true) {
                    override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                        return first.movePointRight(second.toBigInteger().intValue())
                    }
                })
                it("should operate like a regular operator") {
                    shouldEqual("212.34", expression.eval().toPlainString())
                }
            }

            on("adding a custom unary operator") {
                val expression = ExpressionParser("~5")
                expression.addUnaryOperator(object : UnaryOperator('~') {
                    override fun apply(number: BigDecimal): BigDecimal {
                        return number + BigDecimal.ONE
                    }

                })
                it("should operate like a regular unary operator") {
                    shouldEqual("6", expression.eval().toPlainString())
                }
            }

            on("adding a custom function") {
                val expression = ExpressionParser("2 * average(12,4,8)")
                expression.addFunction(object : Function("AVERAGE", 3) {
                    override fun apply(parameters: List<BigDecimal>): BigDecimal {
                        val sum = (parameters[0] + parameters[1]) + parameters[2]
                        return sum / BigDecimal(3)
                    }
                })
            }
        }
    }
}