package com.yoavst.algebra

import com.yoavst.algebra.parser.ExpressionParser
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.shouldEqual
import java.math.BigDecimal
import java.math.MathContext


public class VariablesTest: Spek() {
    init {
        given("an expression evaluator") {
            on("Trying to parse expressions with variables") {
                it("should insert them correctly") {
                    shouldEqual("3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679",  ExpressionParser("PI").setPrecision(MathContext.UNLIMITED.getPrecision()).eval().toString())
                    shouldEqual("3.141592653589793238462643383279503",  ExpressionParser("PI").setPrecision(MathContext.DECIMAL128.getPrecision()).eval().toString())
                    shouldEqual("3.141592653589793",  ExpressionParser("PI").setPrecision(MathContext.DECIMAL64.getPrecision()).eval().toString())
                    shouldEqual("3.141593",  ExpressionParser("PI").setPrecision(MathContext.DECIMAL32.getPrecision()).eval().toString())
                    shouldEqual("6.283186",  ExpressionParser("PI*2.0").eval().toString())
                    shouldEqual("21",
                             ExpressionParser("3*x").addConstant("x", BigDecimal("7"))
                                    .eval().toString())
                    shouldEqual(
                            "20",
                             ExpressionParser("(a^2)+(b^2)")
                                    .with("a",  BigDecimal("2"))
                                    .with("b",  BigDecimal("4")).eval()
                                    .toPlainString())
                    shouldEqual(
                            "68719480000",
                             ExpressionParser("a^(2+b)^2")
                                    .with("a", "2")
                                    .with("b", "4").eval()
                                    .toPlainString())

                    val e = ExpressionParser("x+y")
                    shouldEqual("2", e.with("x", "1").and("y", "1").eval().toPlainString())
                    shouldEqual("1", e.with("y", "0").eval().toPlainString())
                    shouldEqual("0", e.with("x", "0").eval().toPlainString())
                    shouldEqual("21",
                            ExpressionParser("3*longname").with("longname", BigDecimal("7"))
                                    .eval().toString())

                    shouldEqual("21",
                             ExpressionParser("3*longname1").with("longname1",  BigDecimal("7"))
                                    .eval().toString())

                    shouldEqual("21",
                             ExpressionParser("3*_longname1").with("_longname1",  BigDecimal("7"))
                                    .eval().toString())


                }
            }
        }
    }
}