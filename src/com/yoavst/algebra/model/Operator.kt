package com.yoavst.algebra.model

import java.math.BigDecimal


public abstract class Operator(public val operator: String, public val order: Int, public val isLeftAssociative: Boolean = true) {
    public abstract fun apply(first: BigDecimal, second: BigDecimal): BigDecimal
}