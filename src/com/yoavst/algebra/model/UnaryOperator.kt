package com.yoavst.algebra.model

import java.math.BigDecimal

/**
 * A unary operation is an operation with only one operand, i.e. a single input.
 */
public abstract class UnaryOperator(public val operator: Char) {
    public abstract fun apply(number: BigDecimal): BigDecimal
}