package com.yoavst.algebra.model

import java.math.BigDecimal

public abstract class Function(public val name: String, public val parameters: Int) {
    public abstract fun apply(parameters: List<BigDecimal>): BigDecimal
}