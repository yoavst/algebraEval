package com.yoavst.algebra.parser

import com.yoavst.algebra.model.*
import com.yoavst.algebra.model.Function
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.math.RoundingMode
import java.util.*
import kotlin.math.minus

public class ExpressionParser {
    val expression: String
    val operators: MutableMap<String, Operator> = HashMap(20)
    val unaryOperators: MutableMap<Char, UnaryOperator> = HashMap(6)
    val functions: MutableMap<String, Function> = HashMap(30)
    val constants: MutableMap<String, BigDecimal> = HashMap(7)

    var mathContext: MathContext = MathContext.DECIMAL32

    public constructor(expression: String, shouldInitOperators: Boolean = true, shouldInitFunctions: Boolean = true, shouldInitConstants: Boolean = true) {
        if (shouldInitOperators) {
            addOperators()
            addUnaryOperators()
        }
        if (shouldInitFunctions)
            addFunctions()
        if (shouldInitConstants)
            addConstants()
        this.expression = expression.trim()
    }

    private fun addOperators() {
        addOperator(object : Operator("+", 20, true) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return first.add(second, mathContext)
            }
        })

        addOperator(object : Operator("-", 20, true) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return first.subtract(second, mathContext)
            }
        })

        addOperator(object : Operator("*", 30, true) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return first.multiply(second, mathContext)
            }
        })

        addOperator(object : Operator("/", 30, true) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return first.divide(second, mathContext)
            }
        })

        addOperator(object : Operator("%", 30, true) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return first.remainder(second, mathContext)
            }
        })

        addOperator(object : Operator("^", 40, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                var newSecond = second
                /*-
				 * Thanks to Gene Marin:
				 * http://stackoverflow.com/questions/3579779/how-to-do-a-fractional-power-on-bigdecimal-in-java
				 */
                val signOf2 = newSecond.signum()
                val dn1 = first.doubleValue()
                newSecond = newSecond.multiply(BigDecimal(signOf2)) // n2 is now indexitive
                val remainderOf2 = newSecond.remainder(BigDecimal.ONE)
                val n2IntPart = newSecond.subtract(remainderOf2)
                val intPow = first.pow(n2IntPart.intValueExact(), mathContext)
                val doublePow = BigDecimal(Math.pow(dn1, remainderOf2.doubleValue()))

                var result = intPow.multiply(doublePow, mathContext)
                if (signOf2 == -1) {
                    result = BigDecimal.ONE.divide(result, mathContext.getPrecision(), RoundingMode.HALF_UP)
                }
                return result
            }
        })

        addOperator(object : Operator("&&", 4, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                val b1 = first != BigDecimal.ZERO
                val b2 = second != BigDecimal.ZERO
                return if (b1 && b2) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("||", 2, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                val b1 = first != BigDecimal.ZERO
                val b2 = second != BigDecimal.ZERO
                return if (b1 || b2) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator(">", 10, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) == 1) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator(">=", 10, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) >= 0) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("<", 10, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) == -1)
                    BigDecimal.ONE
                else
                    BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("<=", 10, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) <= 0) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("=", 7, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) == 0) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("==", 7, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return operators["="]!!.apply(first, second)
            }
        })

        addOperator(object : Operator("!=", 7, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return if (first.compareTo(second) != 0) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addOperator(object : Operator("<>", 7, false) {
            override fun apply(first: BigDecimal, second: BigDecimal): BigDecimal {
                return operators["!="]!!.apply(first, second)
            }
        })
    }

    private fun addUnaryOperators() {
        addUnaryOperator(object : UnaryOperator('+') {
            override fun apply(number: BigDecimal): BigDecimal = number
        })
        addUnaryOperator(object : UnaryOperator('-') {
            override fun apply(number: BigDecimal): BigDecimal = -number
        })
    }

    private fun addFunctions() {
        addFunction(object : Function("NOT", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val zero = parameters.get(0).compareTo(BigDecimal.ZERO) == 0
                return if (zero) BigDecimal.ONE else BigDecimal.ZERO
            }
        })

        addFunction(object : Function("IF", 3) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val isTrue = parameters.get(0) != BigDecimal.ZERO
                return if (isTrue) parameters.get(1) else parameters.get(2)
            }
        })

        addFunction(object : Function("RANDOM", 0) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.random()
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("SIN", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.sin(Math.toRadians(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("COS", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.cos(Math.toRadians(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("TAN", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.tan(Math.toRadians(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("ASIN", 1) { // added by av
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.toDegrees(Math.asin(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("ACOS", 1) { // added by av
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.toDegrees(Math.acos(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("ATAN", 1) { // added by av
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.toDegrees(Math.atan(parameters.get(0).doubleValue()))
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("SINH", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.sinh(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("COSH", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.cosh(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("TANH", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.tanh(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("RAD", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.toRadians(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("DEG", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.toDegrees(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("MAX", 2) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val v1 = parameters.get(0)
                val v2 = parameters.get(1)
                return if (v1.compareTo(v2) > 0) v1 else v2
            }
        })
        addFunction(object : Function("MIN", 2) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val v1 = parameters.get(0)
                val v2 = parameters.get(1)
                return if (v1.compareTo(v2) < 0) v1 else v2
            }
        })
        addFunction(object : Function("ABS", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                return parameters.get(0).abs(mathContext)
            }
        })
        addFunction(object : Function("LOG", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.log(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("LOG10", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val d = Math.log10(parameters.get(0).doubleValue())
                return BigDecimal(d, mathContext)
            }
        })
        addFunction(object : Function("ROUND", 2) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val toRound = parameters.get(0)
                val precision = parameters.get(1).intValue()
                return toRound.setScale(precision, mathContext.getRoundingMode())
            }
        })
        addFunction(object : Function("FLOOR", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val toRound = parameters.get(0)
                return toRound.setScale(0, RoundingMode.FLOOR)
            }
        })
        addFunction(object : Function("CEILING", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                val toRound = parameters.get(0)
                return toRound.setScale(0, RoundingMode.CEILING)
            }
        })
        addFunction(object : Function("SQRT", 1) {
            public override fun apply(parameters: List<BigDecimal>): BigDecimal {
                /*
				 * From The Java Programmers Guide To numerical Computing
				 * (Ronald Mak, 2003)
				 */
                val x = parameters.get(0)
                if (x.compareTo(BigDecimal.ZERO) == 0) {
                    return BigDecimal(0)
                }
                if (x.signum() < 0) {
                    throw IllegalArgumentException("Argument to SQRT() function must not be negative")
                }
                val n = x.movePointRight(mathContext.getPrecision() shl 1).toBigInteger()

                val bits = (n.bitLength() + 1) shr 1
                var ix = n.shiftRight(bits)
                val ixPrev: BigInteger

                do {
                    ixPrev = ix
                    ix = ix.add(n.divide(ix)).shiftRight(1)
                    // Give other threads a chance to work;
                    Thread.yield()
                } while (ix.compareTo(ixPrev) != 0)

                return BigDecimal(ix, mathContext.getPrecision())
            }
        })
    }

    private fun addConstants() {
        addConstant("PI", Pi)
        addConstant("TRUE", BigDecimal.ONE)
        addConstant("FALSE", BigDecimal.ZERO)
    }

    public fun eval(): BigDecimal {
        val stack = Stack<BigDecimal>()
        for (token in toRpn()) {
            when (token.type) {
                TokenType.Number -> {
                    stack.push(BigDecimal(token.text, mathContext))
                }
                TokenType.Operator -> {
                    val v1 = stack.pop()
                    val v2 = stack.pop()
                    stack.push(operators[token.text]!!.apply(v2, v1))
                }
                TokenType.UnaryOperator -> {
                    val v1 = stack.pop()
                    stack.push(unaryOperators[token.text[0]]!!.apply(v1))
                }
                TokenType.Name -> {
                    if (token.text in constants.keySet()) {
                        val value = constants[token.text]!!
                        stack.push(value.round(mathContext))
                    } else if (token.text.toUpperCase() in functions.keySet()) {
                        val f = functions[token.text.toUpperCase()]!!
                        val list = ArrayList<BigDecimal>(f.parameters)
                        for (i in 0..f.parameters - 1) {
                            list.add(0, stack.pop())
                        }
                        val result = f.apply(list)
                        stack.push(result)
                    }
                }
            }
        }
        return stack.pop().stripTrailingZeros()
    }

    public fun getTokenizer(): Tokenizer = Tokenizer(expression, unaryOperators, operators)

    public fun toRpn(): List<Token> {
        val outputQueue = ArrayList<Token>()
        val stack = Stack<Token>()
        val tokenizer = getTokenizer()

        var lastFunction: Token? = null
        var previousToken: Token? = null
        while (tokenizer.hasNext()) {
            val token = tokenizer.next()
            when (token.type) {
                TokenType.Number -> {
                    outputQueue.add(token)
                }
                TokenType.Name -> {
                    if (token.text.toUpperCase() in functions.keySet()) {
                        if (previousToken != null && (previousToken.type == TokenType.Number
                                || (previousToken.type == TokenType.Name && previousToken.text.toUpperCase() !in functions.keySet()))) {
                            while (stack.isNotEmpty() && stack.peek().type == TokenType.UnaryOperator) outputQueue.add(stack.pop())
                            stack.push(Token(TokenType.Operator, "*"))
                        }
                        stack.push(token)
                        lastFunction = token
                    } else if (token.text.toUpperCase() in constants.keySet()) {
                        outputQueue.add(token)
                    } else {
                        if (previousToken != null && previousToken.type == TokenType.Number) {
                            while (stack.isNotEmpty() && stack.peek().type == TokenType.UnaryOperator) outputQueue.add(stack.pop())
                            outputQueue.add(token)
                            outputQueue.add(Token(TokenType.Operator, "*"))
                        } else outputQueue.add(token)

                    }
                }
                TokenType.Comma -> {
                    while (!stack.isEmpty() && stack.peek().type != TokenType.LeftParen) {
                        outputQueue.add(stack.pop())
                    }
                    if (stack.isEmpty()) {
                        throw IllegalArgumentException("Parse error for function '${lastFunction?.text}'");
                    }
                }
                TokenType.Operator -> {
                    val o1 = operators[token.text]!!
                    var token2 = if (stack.isEmpty()) null else stack.peek()
                    while (token2 != null && (isOperatorWithHigherOrder(o1, token2) || token2.type == TokenType.UnaryOperator)) {
                        outputQueue.add(stack.pop())
                        token2 = if (stack.isEmpty()) null else stack.peek()
                    }
                    stack.push(token)
                }
                TokenType.LeftParen -> {
                    if (previousToken != null) {
                        if (previousToken.type == TokenType.Number || (previousToken.type == TokenType.Name && previousToken.text.toUpperCase() !in functions.keySet())) {
                            while (stack.isNotEmpty() && stack.peek().type == TokenType.UnaryOperator) outputQueue.add(stack.pop())

                            stack.push(Token(TokenType.Operator, "*"))
                        }
                    }
                    stack.push(token);
                }
                TokenType.RightParen -> {
                    while (stack.isNotEmpty() && stack.peek().type != TokenType.LeftParen) {
                        outputQueue.add(stack.pop())
                    }
                    if (stack.isEmpty()) {
                        throw IllegalArgumentException("Mismatched parentheses")
                    }
                    stack.pop();
                    if (stack.isNotEmpty() && stack.peek().text.toUpperCase() in functions.keySet()) {
                        outputQueue.add(stack.pop())
                    }
                    while (stack.isNotEmpty() && stack.peek().type == TokenType.UnaryOperator) {
                        outputQueue.add(stack.pop())
                    }
                }
                TokenType.UnaryOperator -> {
                    stack.push(token)
                }
            }
            previousToken = token
        }
        while (stack.isNotEmpty()) {
            val token = stack.pop()
            if (token.type == TokenType.LeftParen || token.type == TokenType.RightParen) {
                throw IllegalArgumentException("Mismatched parentheses")
            } else if (token.type != TokenType.Operator && token.type != TokenType.UnaryOperator) {
                throw RuntimeException("Unknown operator or function: ${token.text}");
            }
            outputQueue.add(token)
        }
        return outputQueue
    }

    private fun isOperatorWithHigherOrder(o1: Operator, token2: Token): Boolean =
            token2.type == TokenType.Operator && ((o1.isLeftAssociative && o1.order <= operators[token2.text]!!.order) ||
                    (o1.order < operators[token2.text]!!.order))

    public fun toRpnString(): String = (toRpn() map { it.text }).join(separator = " ")


    public fun addOperator(operator: Operator): ExpressionParser {
        operators[operator.operator] = operator
        return this
    }

    public fun addUnaryOperator(operator: UnaryOperator): ExpressionParser {
        unaryOperators[operator.operator] = operator
        return this
    }

    public fun addFunction(function: Function): ExpressionParser {
        functions[function.name] = function
        return this
    }

    public fun addConstant(constant: String, value: BigDecimal): ExpressionParser {
        constants[constant] = value
        return this
    }

    public fun with(constant: String, value: BigDecimal): ExpressionParser {
        return addConstant(constant, value)
    }

    public fun and(variable: String, value: String): ExpressionParser {
        return addConstant(variable, BigDecimal(value))
    }

    public fun and(variable: String, value: BigDecimal): ExpressionParser {
        return addConstant(variable, value)
    }

    public fun with(variable: String, value: String): ExpressionParser {
        return addConstant(variable, BigDecimal(value))
    }

    public fun setPrecision(precision: Int): ExpressionParser {
        mathContext = MathContext(precision)
        return this
    }

    public fun setRoundingMode(roundingMode: RoundingMode): ExpressionParser {
        mathContext = MathContext(mathContext.getPrecision(), roundingMode)
        return this
    }

    companion object {
        public val Pi: BigDecimal = BigDecimal("3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679")
        public val DecimalSeparator: Char = '.'
    }
}