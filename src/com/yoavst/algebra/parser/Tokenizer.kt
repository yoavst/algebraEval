package com.yoavst.algebra.parser

import com.yoavst.algebra.model.Operator
import com.yoavst.algebra.model.UnaryOperator


public class Tokenizer(private val input: String, private val unaryOperators: Map<Char, UnaryOperator>,
                       private val operators: Map<String, Operator>) : Iterator<Token> {
    private var previousToken: Token? = null
    public var index: Int = 0
    override fun next(): Token {
        if (index >= input.length()) throw IllegalStateException()
        val token = StringBuilder()
        var char = input[index]
        while (char.isWhitespace() && index < input.length()) {
            char = input[++index]
        }
        if (char.isDigit()) {
            while ((Character.isDigit(char) || char == ExpressionParser.DecimalSeparator) && (index < input.length())) {
                token.append(input.charAt(index++))
                char = if (index == input.length()) ' ' else input[index]
            }
            previousToken = Token(TokenType.Number, token.toString())
            return previousToken!!
        } else if (char in unaryOperators.keySet() && !peek().isWhitespace()
                && (previousToken == null || "(" == previousToken!!.text || "," == previousToken!!.text
                || previousToken!!.text in operators.keySet() || previousToken!!.text in unaryOperators.keySet())) {
            token.append(char)
            index++
            previousToken = Token(TokenType.UnaryOperator, token.toString())
            return previousToken!!
        } else if (char.isLetter() || char == '_') {
            while ((char.isLetter() || char.isDigit() || char == '_') && index < input.length()) {
                token.append(input[index++])
                char = if (index == input.length()) ' ' else input[index]
            }
            previousToken = Token(TokenType.Name, token.toString())
            return previousToken!!
        } else if (char == '(') {
            index++
            previousToken = Token(TokenType.LeftParen, "(")
            return previousToken!!
        } else if (char == ')') {
            index++
            previousToken = Token(TokenType.RightParen, ")")
            return previousToken!!
        } else if (char == ',') {
            index++
            previousToken = Token(TokenType.Comma, ",")
            return previousToken!!
        } else {
            while (!char.isLetter() && !char.isDigit() && char != '_' && !Character.isWhitespace(char)
                    && char != '(' && char != ')' && char != ',' && index < input.length()) {
                token.append(input[index])
                index++
                char = if (index == input.length()) ' ' else input[index]
                if (char in unaryOperators) {
                    break
                }
            }
            if (token.toString() !in operators.keySet()) {
                throw IllegalArgumentException("Unknown operator '" + token + "' at indexition " + (index - token.length() + 1))
            }
            previousToken = Token(TokenType.Operator, token.toString())
            return previousToken!!
        }
    }

    override fun hasNext(): Boolean = index != input.length()

    /**
     * Return the next char that is not a space, or a space if reached the end.
     */
    private fun peek(): Char {
        var position = index
        var char: Char
        while (position < input.length()) {
            char = input[position]
            if (!char.isWhitespace())
                return char
            position++
        }
        return ' '
    }
}
