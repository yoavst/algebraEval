package com.yoavst.algebra.parser


public data class Token(public val type: TokenType, public val text: String)