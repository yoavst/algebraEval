package com.yoavst.algebra.parser


public enum class TokenType {
    Operator,
    UnaryOperator,
    Name,
    LeftParen,
    RightParen,
    Comma,
    Number
}